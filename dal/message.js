'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var DatabaseResponse = require('../common/db_response.js').DatabaseResponse;
var _ = require('underscore');

var MessageSchema = new Schema({
        to: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        from: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        message: {type: String},
        chat: {type: mongoose.Schema.Types.ObjectId, ref: 'Chat'},
        isRead: {type: Number}
    }, {timestamps: true}
);

MessageSchema.plugin(mongoosePaginate);
var message = mongoose.model('Message', MessageSchema);

function findAllWithPagination(searchParams, options, callback) {
    message.paginate(searchParams, options, function (err, users) {
        if (err) {
            var databaseResponse = new DatabaseResponse(err, null);
            return callback(databaseResponse);
        }
        var databaseResponse = new DatabaseResponse(null, users);
        return callback(databaseResponse);
    });
};

function findAll(searchParams, callback) {
    message.find(searchParams).exec(function (err, users) {
        if (err) {
            var databaseResponse = new DatabaseResponse(err, null);
            return callback(databaseResponse);
        }
        var databaseResponse = new DatabaseResponse(null, users);
        return callback(databaseResponse);
    });
}

function Message() {

    Message.prototype.findById = function (id, callback) {
        message.find({
            _id: id
        }, function (err, message) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, message);
            callback(databaseResponse);
        });
    };

    Message.prototype.findOne = function (params, callback) {
        message.findOne(params, function (err, message) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, message);
            callback(databaseResponse);
        });
    };

    Message.prototype.findAll = function (searchParams, options, callback) {
        if (!_.isEmpty(options)) {
            findAllWithPagination(searchParams, options, callback);
        } else {
            findAll(searchParams, callback);
        }
    };

    Message.prototype.create = function (messageInfo, callback) {
        var newMessage = new message(messageInfo);
        newMessage.save(function (err, message) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, message);
            callback(databaseResponse);
        });
    };

    Message.prototype.update = function (messageInfo, data, callback) {
        message.findOneAndUpdate({_id: messageInfo.id}, data, {new: true}, function (err, message) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, message);
            callback(databaseResponse);
        });
    };

    Message.prototype.delete = function (id, callback) {
        message.find({
            _id: id
        }, function (err, message) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            message.remove(function (err) {
                if (err) {
                    var databaseResponse = new DatabaseResponse(err, null);
                    return callback(databaseResponse);
                }
                var databaseResponse = new DatabaseResponse(null, {});
                callback(databaseResponse);
            });
        });
    };

};

exports.Message = Message;