'use strict';

var ChatModel = require('../models/chat.js').ChatModel;
var chatRepository = new (require('../repository/chat.js')).ChatRepository();
var messageRepository = new (require('../repository/message.js')).MessageRepository();
var validator = new (require('../validations/validator_request.js')).Validator();
var ServerResponse = require('../common/server_response.js').ServerResponse;
var ResponseCodes = require('../common/constant.js').ResponseCodes;
var Messages = require('../common/constant.js').Messages;
var queryHelper = new (require('../helper/query.js')).QueryHelper();

function ChatService() {

    ChatService.prototype.getChats = function (data, callback) {

        var options = queryHelper.getPaginationParams(data);
        options.sort = {
            createdAt: -1
        };

        var searchParams = {
            users: data.userId
        };

        chatRepository.findAll(searchParams, options, function (databaseResponse) {
            if (!databaseResponse.error) {
                var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                callback(response);
            } else {
                var response = new ServerResponse(databaseResponse.error, ResponseCodes.InternalServerError, Messages.Error);
                callback(response);
                return;
            }
        });
    };

    ChatService.prototype.getChat = function (chatId, callback) {

        chatRepository.findById(chatId, function (databaseResponse) {
            if (!databaseResponse.error) {
                var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                callback(response);
            } else {
                var response = new ServerResponse(databaseResponse.error, ResponseCodes.InternalServerError, Messages.Error);
                callback(response);
                return;
            }
        });
    };

    ChatService.prototype.getChatMessages = function (data, callback) {

        var searchParams = {
            users: {'$all': data.user}
        };

        chatRepository.findOne(searchParams, function (databaseResponse) {
            if (!databaseResponse.error) {
                if (databaseResponse.data && databaseResponse.data._doc) {
                    var options = queryHelper.getPaginationParams(data);
                    options.sort = {
                        createdAt: -1
                    };

                    var searchParams = {
                        chat: databaseResponse.data._doc._id
                    };

                    messageRepository.findAll(searchParams, options, function (databaseResponse) {
                        if (!databaseResponse.error) {
                            var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                            callback(response);
                        } else {
                            var response = new ServerResponse(databaseResponse.error, ResponseCodes.BadRequest, Messages.Error);
                            callback(response);
                        }
                    });
                } else {
                    var response = new ServerResponse({}, ResponseCodes.Ok, Messages.Success);
                    callback(response);
                }

            } else {
                var response = new ServerResponse(databaseResponse.error, ResponseCodes.BadRequest, Messages.Error);
                callback(response);
            }
        });
    };

    ChatService.prototype.findOrCreateChat = function (data, callback) {

        var chatModel = new ChatModel();
        var chatInfo = chatModel.chatInfo(data);
        var chatInfoRA = chatModel.chatInfoRA();

        console.log(chatInfo);
        console.log("*****************");
        console.log(chatInfoRA);

        var result = validator.validate(chatInfo, chatInfoRA);

        console.log('Validation error -- ' + JSON.stringify(result));

        if (!!result && result.validationResult.length > 0) {
            if (callback) {
                callback(result);
            } else {
                throw result;
            }
        } else {

            chatRepository.findOne({
                users: {
                    $all: data.users
                }
            }, function (databaseResponse) {
                if (!databaseResponse.error) {

                    if (databaseResponse.data) {

                        var chatModel = new ChatModel();

                        var chatUpdateData = chatModel.chatUpdateInfo(databaseResponse.data, data);

                        chatRepository.update({id: databaseResponse.data._id}, chatUpdateData, function (databaseResponse) {
                            return callback(databaseResponse);
                        });
                    } else {
                        chatRepository.create(chatInfo, function (databaseResponse) {
                            return callback(databaseResponse);
                        });
                    }

                } else {
                    return callback(databaseResponse);
                }
            });
        }
    };

    ChatService.prototype.markAsRead = function (data, callback) {

        var chatModel = new ChatModel();

        var chatInfo = chatModel.markAsRead(data);
        var chatInfoRA = chatModel.markAsReadRA();
        var result = validator.validate(chatInfo, chatInfoRA);

        console.log('Validation error -- ' + JSON.stringify(result));

        if (!!result && result.validationResult.length > 0) {
            if (callback) {
                callback(result);
            } else {
                throw result;
            }
        } else {

            chatRepository.findById(data.chatId, function (databaseResponse) {

                if (!databaseResponse.error) {
                    var updatedChatInfo = chatModel.getUnReadObject(chatInfo, databaseResponse.data)
                    chatRepository.update({id: data.chatId}, updatedChatInfo, function (databaseResponse) {
                        if (!databaseResponse.error) {
                            var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                            callback(response);
                        } else {
                            var response = new ServerResponse(databaseResponse.error, ResponseCodes.InternalServerError, Messages.Error);
                            callback(response);
                            return;
                        }
                    });
                } else {
                    var response = new ServerResponse(databaseResponse.error, ResponseCodes.InternalServerError, Messages.Error);
                    callback(response);
                    return;
                }
            });
        }
    };

    ChatService.prototype.getUnReadMessagesCount = function (data, callback) {
        var chatModel = new ChatModel();

        var getUnReadChatCountInfo = chatModel.getUnReadChatCountInfo(data);
        var getUnReadChatCountInfoRA = chatModel.getUnReadChatCountInfoRA();
        var result = validator.validate(getUnReadChatCountInfo, getUnReadChatCountInfoRA);

        console.log('Validation error -- ' + JSON.stringify(result));

        if (!!result && result.validationResult.length > 0) {
            if (callback) {
                callback(result);
            } else {
                throw result;
            }
        } else {

            chatRepository.getCount(getUnReadChatCountInfo, function (databaseResponse) {

                if (!databaseResponse.error) {
                    var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                    callback(response);
                } else {
                    var response = new ServerResponse(databaseResponse.error, ResponseCodes.InternalServerError, Messages.Error);
                    callback(response);
                    return;
                }
            });
        }
    }

}
exports.ChatService = ChatService;