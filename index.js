var express = require('express');
var https = require('http');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var app = express();
var session = require('express-session');
// var mongo_uri="mongodb://hamzox:beanar123@ds129706.mlab.com:29706/development-beanar";
var passport = require('passport');
var utilityHelper = require('./helper/utility.js').intance();
var cors = require('cors');
var sock = require('socket.io');
var mongo_uri = {
  staging: "mongodb://hamzox:beanar123@ds129706.mlab.com:29706/staging-beanar",
  development: "mongodb://hamzox:beanar123@ds129706.mlab.com:29706/development-beanar"
}

app.set('port', (process.env.PORT || 5000));
mongoose.Promise = global.Promise;
mongoose.connect(mongo_uri.staging, function (error) {
    if (error) console.error(error);
    else console.log('mongo connected to beanar');
});

require('./helper/passport.js');
app.use(passport.initialize()); 
app.use(passport.session());
app.use(function(req, res, next) {
    res.locals.user = req.user;
    next();
});
  app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json()); // use body-parser middleware
app.use(express.static(__dirname + '/public'));
app.use(cors());


app.use('/api', require('./routes/api'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

// app.listen(app.get('port'), function() {
//   console.log('Node app is running on port', app.get('port'));
// });

console.log(app.get('port'));
/* Start the server */
var httpsServer = https.createServer(app);
httpsServer.listen(app.get('port'), function () {
    console.log('listening on port', app.get('port'));
});

/* Initialize socket server */
var io = sock.listen(httpsServer);
require('./routes/socket.js')(io);