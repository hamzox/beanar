
const UserModel = require('../models/user.js').UserModel;
const userRepository = new (require('../repository/user.js')).UserRepository();
const ServerResponse = require('../common/server_response.js').ServerResponse;
const ResponseCodes = require('../common/constant.js').ResponseCodes;
const Messages = require('../common/constant.js').Messages;
const DBErrorCodes = require('../common/constant.js').DBErrorCodes;
const validator = new (require('../validations/validator_request.js')).Validator();
const bcryptHelper = new (require('../helper/bcrypt.js')).BcryptHelper();
const _ = require('underscore');

function UserService() {

    function getLinearObject(records) {
        var newRecord = {};
        _.each(records, function (value, key, records) {
            if (value instanceof Object) {
                _.each(value, function (subValue, subKey, records) {
                    newRecord[subKey] = subValue;
                });
            } else {
                newRecord[key] = value;
            }
        });

        return newRecord;
    };

    function updateUserInfo(id, userInfo, callback) {
        userRepository.update({id: id}, userInfo, function (databaseResponse) {
            if (!databaseResponse.error) {
                var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                callback(response);
            } else if (databaseResponse.error.code == DBErrorCodes.DuplicateError) {
                var response = new ServerResponse({}, ResponseCodes.Conflict, Messages.Conflict);
                callback(response);
            } else {
                var response = new ServerResponse(databaseResponse.error, ResponseCodes.BadRequest, Messages.Error);
                callback(response);
            }
        });
    };

    UserService.prototype.registerUser = function(data, callback) {
        var userModel = new UserModel();
        var userInfo = userModel.userInfo(data);
        var userInfoRA = userModel.userInfoRA();
        var result = validator.validate(userInfo, userInfoRA);

        if (!!result && result.validationResult.length > 0) {
            if (callback) {
                callback(result);
            } else {
                throw result;
            }
        }
        else { //save the user data to the db with password encrypted.
            bcryptHelper.encryptPassword(userInfo.password, function(hashPassword, err) {
                if(!err){
                    userInfo.password = hashPassword;
                    userRepository.create(userInfo, function (databaseResponse) {
                        if (!databaseResponse.error) {
                            var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                            callback(response);
                        } else if (databaseResponse.error.code == DBErrorCodes.DuplicateError) {
                            var response = new ServerResponse({}, ResponseCodes.Conflict, Messages.Conflict);
                            callback(response);
                        } else {
                            var response = new ServerResponse(err, ResponseCodes.BadRequest, Messages.Error);
                            callback(response);
                        }
                    });
                } else {
                    var response = new ServerResponse(err, ResponseCodes.InternalServerError, Messages.Error);
                    callback(response);
                }
            });
        }
    }

    UserService.prototype.updateUserToken = function (userInfo, token, callback) {
        userRepository.update(userInfo, {token: token}, function (databaseResponse) {
            return callback(databaseResponse);
        });
    };  

    UserService.prototype.getUser = function (data, callback) {
        userRepository.findOne(data, function (databaseResponse) {
            return callback(databaseResponse);
        });
    };

    UserService.prototype.validateLogin = function (userInfo, callback) {
        var userModel = new UserModel();
        var userInfo = userModel.login(userInfo);
        var userInfoRA = userModel.loginRA();
        var result = validator.validate(userInfo, userInfoRA);
        if (!!result && result.validationResult.length > 0) {
            callback(false, result);
        } else {
            callback(true, null);
        }
    };

    UserService.prototype.getUserById = function (data, callback) {
        userRepository.findById(data, function (databaseResponse) {
            if (!databaseResponse.error) {
                var response = new ServerResponse(databaseResponse.data, ResponseCodes.Ok, Messages.Success);
                callback(response);
            } else {
                return callback({}, err);
            }
        });
    };

    UserService.prototype.updateUserSocketInfo = function (data, callback) {

        var userModel = new UserModel();
        var userSocketInfo = userModel.userSocketInfo(data);
        var userSocketInfoRA = userModel.userSocketInfoRA();
        var result = validator.validate(userSocketInfo, userSocketInfoRA);

        console.log('Validation error -- ' + JSON.stringify(result));

        if (!!result && result.validationResult.length > 0) {
            if (callback) {
                callback(result);
            } else {
                throw result;
            }
        } else {
            userRepository.update({id: data.id}, userSocketInfo, function (databaseResponse) {
                return callback(databaseResponse);
            });
        }
    };

    UserService.prototype.logout = function (data, callback) {
        if (!_.isEmpty(data._id)) {
            var userInfo = {
                token: '',
                socket: ''
                /** Uncomment this section later when chat/messages is implemented
                isActive: false,
                deviceInfo: {
                    fcmId: ''
                }
                **/
            };
            userRepository.update({id: data._id}, userInfo, function (databaseResponse) {
                if (!databaseResponse.error) {
                    var response = new ServerResponse({}, ResponseCodes.Ok, Messages.Success);
                    callback(response);
                } else {
                    var response = new ServerResponse(databaseResponse.error, ResponseCodes.BadRequest, Messages.Error);
                    callback(response);
                }
            });
        } else {
            var response = new ServerResponse("Id is required", ResponseCodes.BadRequest, Messages.Error);
            callback(response);
        }
    };

    UserService.prototype.updateUser = function (data, callback) {

        userRepository.findById(data._id, function (databaseResponse) {
            if (!databaseResponse.error && !_.isEmpty(databaseResponse.data)) {
                var userModel = new UserModel();
                var userInfo = userModel.descriptiveUserInfo(getLinearObject(databaseResponse.data.toObject()));
                var updateUserData = userModel.updateUserInfo(userInfo, data);
                updateUserData.characterInfo = (updateUserData.characterInfo instanceof Array) ? updateUserData.characterInfo[0] : updateUserData.characterInfo;
                updateUserData.details = (updateUserData.details instanceof Array) ? updateUserData.details[0] : updateUserData.details;
                updateUserData.roomDetails = (updateUserData.roomDetails instanceof Array) ? updateUserData.roomDetails[0] : updateUserData.roomDetails;

                if (!_.isEmpty(updateUserData.password)) {
                    bcryptHelper.encryptPassword(updateUserData.password, function (hashPassword, err) {
                        if (!err) {
                            updateUserData.password = hashPassword;
                            updateUserInfo(data._id, updateUserData, callback);

                        } else {
                            var response = new ServerResponse(err, ResponseCodes.InternalServerError, Messages.Error);
                            callback(response);
                        }
                    });
                } else {
                    updateUserInfo(data._id, updateUserData, callback);
                }



            } else {
                var response = new ServerResponse(databaseResponse.error, ResponseCodes.BadRequest, Messages.Error);
                callback(response);
            }
        });
    };
}

exports.UserService = UserService;