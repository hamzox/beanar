'use strict';

var chat = new (require('../dal/chat.js')).Chat();

function ChatRepository() {

    ChatRepository.prototype.findById = function (id, callback) {
        try {
            chat.findById(id, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.findOne = function (params, callback) {
        try {
            chat.findOne(params, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.findAll = function (searchParams, options, callback) {
        try {
            chat.findAll(searchParams, options, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.getList = function (callback) {
        try {
            chat.getList(function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.create = function (chatInfo, callback) {
        try {
            chat.create(chatInfo, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.update = function (chatInfo, data, callback) {
        try {
            chat.update(chatInfo, data, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.delete = function (id, callback) {
        try {
            chat.delete(id, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

    ChatRepository.prototype.getCount = function (data, callback) {
        try {
            chat.getCount(data, function (databaseResponse) {
                callback(databaseResponse);
            });
        } catch (err) {
            throw err;
        }
    };

};

exports.ChatRepository = ChatRepository;