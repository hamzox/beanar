const passport = require('passport');
const UserService = require('../services/user.js').UserService;
const BcryptHelper = require('./bcrypt.js').BcryptHelper;
const LocalStrategy = require('passport-local').Strategy;
const _ = require('underscore');

var bcryptHelper = new BcryptHelper();
var userService = new UserService();

passport.serializeUser = function(user, session, callback) {
    callback(null, user.id);
};

passport.deserializeUser = function(id, session, callback) {
    userService.getUserById(id, function(user, err) {
        callback(err, user);
    });
};

/**
 * Sign in using Email and Password.
 */
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function(email, password, callback) {

    userService.getUser({ email: email.toLowerCase() }, function(response) {
        if (response.error) {
            return callback(response.error);
        } if (!_.isEmpty(response.data)) {
            bcryptHelper.comparePassword(password, response.data.password, function (isMatch, err) {
                if(err){
                    return callback(err);
                }
                if (isMatch){
                    return callback(null, response.data);
                }
                return callback(null, false, "Incorrect Password");
            });
        } else {
            return callback(null, false, "Incorrect Email");
        }
    });
}));