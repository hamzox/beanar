
var ResponseCodes = {
    Ok: 200,
    Created: 201,
    AlreadyReported: 208,
    UnAuthorized: 401,
    Forbidden: 403,
    BadRequest: 400,
    ResourceNotFound: 404,
    Conflict: 409,
    GoneError: 410,
    InternalServerError: 500
};

exports.ResponseCodes = ResponseCodes;

var Messages = {
    UnAuthorized: "UnAuthorized Error",
    Problem: "There has a problem with request, please verify your request input",
    Required: "%s is required",
    Length: "%s should be between %d and %d",
    AlreadyReported: "AlreadyReported",
    Success: "Success",
    GoneError: "GoneError",
    Conflict: "Conflict: Same Data Already Exist",
    Error: "Error",
    Forbidden: "Forbidden",
    BadRequest: "Bad Request"
};

exports.Messages = Messages;

var DBErrorCodes = {
    DuplicateError: 11000
};

exports.DBErrorCodes =DBErrorCodes;