var _ = require('underscore');
var userService = new (require('../services/user.js')).UserService();
var userRepository = new (require('../repository/user.js')).UserRepository();
var chatService = new (require('../services/chat.js')).ChatService();
var messageService = new (require('../services/message.js')).MessageService();

module.exports = function (io) {
    io.on('connection', function (socket) {
        console.log('Connected --- ' + socket.id);

        /**
         * get current user by socket id
         **/
        function getUserInfo(callback) {

            try {
                userService.getUser({socket: socket.id}, function (response) {
                    if (!response.error && !_.isEmpty(response.data)) {
                        return callback(response.data, null);
                    } else {
                        return callback(null, response.error);
                    }
                });
            } catch (e) {
                return callback(null, e);
            }
        }

        /*
         * authenticate user using token
         */
        socket.on('auth', function (token) {
            console.log("Request for Token ----", token);
            try {
                if (token) {
                    userService.getUser({token: token.token}, function (response) {
                        if (!response.error && !_.isEmpty(response.data)) {
                            var data = {
                                id: response.data._id,
                                socket: socket.id,
                                // isActive: (isActive && isActive > 0) ? true : false
                            };
                            console.log("----------------------------");
                            console.log(data);
                            console.log("----------------------------");

                            userService.updateUserSocketInfo(data, function (databaseResponse) {
                                if (!databaseResponse.error) {
                                    socket.emit('auth_successful', databaseResponse.data);
                                    console.log('User Socket Id ----' + socket.id);
                                } else {
                                    socket.emit('auth_error', 'Error');
                                    return console.log('Error', databaseResponse.error);
                                }
                            });

                        } else {
                            socket.emit('auth_error', 'Error');
                            console.log(response.error);
                            return console.log('Error');
                        }
                    });
                } else {
                    socket.emit('auth_error', 'Error');
                    return console.log('Error');
                }
            } catch (e) {
                socket.emit('auth_error', 'Error');
                return console.log('Error');
            }

        });

        /*
         * Messages
         */
        socket.on('message', function (messageInfo) {
            console.log('message', messageInfo);
            getUserInfo(function (userInfo, error) {
                //console.log(userInfo);
                if (!error && userInfo) {
                    var chatData = {
                        createdBy: userInfo._id,
                        lastMessage: messageInfo.message,
                        users: [
                            userInfo._id,
                            messageInfo.to
                        ],
                        to: messageInfo.to,
                        from: userInfo._id
                    };
                    //console.log(chatData);
                    /* Find chat by users
                     * If already exist update last message and unRead count
                     * Else create new chat
                     * */
                    chatService.findOrCreateChat(chatData, function (response) {
                        if (!response.error) {
                            /* create message */
                            var messageData = {
                                to: messageInfo.to,
                                from: userInfo._id,
                                message: messageInfo.message,
                                chat: response.data._id
                            };
                            messageService.create(messageData, function (messageResponse) {
                                if (!messageResponse.error) {
                                    var messageInfo = messageResponse.data;
                                    var send = {
                                        type: 'message',
                                        data: messageInfo
                                    };
                                    console.log(send);
                                    socket.emit('message_success', messageInfo);

                                    console.log('Add message - send ' + JSON.stringify(send));
                                    /* get user to send message to */
                                    userRepository.findById(messageData.to, function (databaseResponse) {
                                        if (!databaseResponse.error) {
                                            io.to(databaseResponse.data.socket).emit('messageReceived', {
                                                from: messageInfo.from,
                                                message: messageInfo.message
                                            }, send);
                                            
                                        } else {
                                            socket.emit('Error', 'Error sending message');
                                        }
                                    });

                                } else {
                                    socket.emit('Error', 'Error sending message', messageResponse.error);
                                }
                            });
                        } else {
                            socket.emit('Error', 'Error sending message', response.error);
                        }
                    });

                } else {
                    socket.emit('Error', 'Error sending message', error);
                }

            });

        });

        /*
         * Send Socket message to other client
         */

        /** 
        
        socket.on('sendMessage', function (userId, message) {

            console.log("*** sendMessage Event ***");

            getUserInfo(function (userInfo, error) {
                console.log(userInfo);
                console.log('messaging: from -- ' + userId + " -- socket id -- " + socket.id + " -- to -- " + userId + " -- message -- " + JSON.stringify(message));

                if (!error && userInfo && userInfo._id) {

                    if (message.socket) {
                        console.log('Sending direct socket message to other user');
                        io.to(message.socket).emit('messageReceived', userInfo, message);
                        return;
                    }

                    userRepository.findById(userId, function (databaseResponse) {
                        if (!databaseResponse.error && databaseResponse.data && databaseResponse.data._id) {
                            console.log('messaging: from ' + userInfo.firstName + ' to ' + databaseResponse.data.firstName);

                            var userData = databaseResponse.data;

                            if (userData.socket && message) {
                                message.socket = userData.socket;
                                io.to(userData.socket).emit('messageReceived', userInfo, message);
                            }

                        } else {
                            console.log('Ending call -- fail to establish call');
                            io.to(userInfo.socket).emit('messageReceived', databaseResponse.data, {
                                type: 'end'
                            });
                        }
                    });
                } else {
                    socket.emit('messageReceived', userId, {type: 'end'});
                }
            });

        });
        
         */
        

        /*
         * Remove socket info from user when disconnected
         */
        var disconnect = function () {
            console.log(socket.id + ' disconnected.');
            userService.getUser({socket: socket.id}, function (response) {
                if (!response.error && !_.isEmpty(response.data)) {
                    var data = {
                        id: response.data._id,
                        socket: ''
                        // isActive: response.data.isActive
                    };

                    userService.updateUserSocketInfo(data, function (databaseResponse) {
                        if (!databaseResponse.error) {
                            socket.emit('disconnect_successful', databaseResponse.data);
                            console.log('User Disconnect Socket Id ----' + socket.id);
                        } else {
                            socket.emit('disconnect_error', 'Error');
                        }
                    });
                } else {
                    socket.emit('disconnect_error', 'Error');
                }
            });

        };

        socket.on('logout', disconnect);
        socket.on('disconnect', disconnect);

    });
}