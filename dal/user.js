var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var DatabaseResponse = require('../common/db_response.js').DatabaseResponse;

var UserSchema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    token: {type: String},
    gender: { type : String, required: true },
    password: { type: String, required: true },
    socket: {type: String},
    details: {
        credits: {type: Number, default: 0},
        status: {type: String, default: ""},
    },
    characterInfo: {
        modelId: {type: Number, default: 0},
        upperBodyTexture: {type: Number, default: 0},
        lowerBodyTexture: {type: Number, default: 0},
        shoesTexture: {type: Number, default: 0},
        skinTexture: {type: Number, default: 0},
        attribute1: {type: Number, default: 0},
        attribute2: {type: Number, default: 0},
        attribute3: {type: Number, default: 0},
        attribute4: {type: Number, default: 0},
        skinTone: {type: Number, default: 0}
    },
    roomDetails: {
        furnitures:[{
            furnitureId: {type: String, default: ""},
            transformation : {
                posX : {type: Number, default: 0}, 
                posY : {type: Number, default: 0}, 
                posZ : {type: Number, default: 0},
                RotX : {type: Number, default: 0}, 
                RotY : {type: Number, default: 0}, 
                RotZ : {type: Number, default: 0}, 
                ScaX : {type: Number, default: 0}, 
                ScaY : {type: Number, default: 0}, 
                ScaZ : {type: Number, default: 0} 
            }  
        }]
    }
}, { timestamps: true });


UserSchema.plugin(mongoosePaginate);
var user = mongoose.model('User', UserSchema);

function User() {
    User.prototype.create = function (userInfo, callback) {
        var newUser = new user(userInfo); //new user data
        newUser.save(function (err, user) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, user); //User Created here.
            callback(databaseResponse);
        });
    };

    User.prototype.findOne = function (params, callback) {
        user.findOne(params, function (err, user) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, user);
            callback(databaseResponse);
        });
    };

    User.prototype.update = function (userInfo, data, callback) {
        var options = {
            new: true,
            fields: "-password"
        };
        user.findOneAndUpdate({_id: userInfo.id}, data, options)
            .exec( function (err, user) {
                if (err) {
                    var databaseResponse = new DatabaseResponse(err, null);
                    return callback(databaseResponse);
                }
                var databaseResponse = new DatabaseResponse(null, user);
                callback(databaseResponse);
            });
    };

    User.prototype.findById = function (id, callback) {
        var options = {
            select: '-token'
        };
        user.find({
            _id: id
        }).select('-token')
            .exec(function (err, user) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, user);
            callback(databaseResponse);
        });
    };

}
exports.User = User;