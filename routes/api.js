const express = require('express');
const passport = require('passport');
const router = express.Router();
var chatService = new (require('../services/chat.js')).ChatService();
const userService = new (require('../services/user.js')).UserService();
const ServerResponse = require('../common/server_response.js').ServerResponse;
const ResponseCodes = require('../common/constant.js').ResponseCodes;
const Messages = require('../common/constant.js').Messages;
const DBErrorCodes = require('../common/constant.js').DBErrorCodes;
const utilityHelper = require('../helper/utility.js').intance();
const BcryptHelper = require('../helper/bcrypt.js').BcryptHelper;
const LocalStrategy = require('passport-local').Strategy;
const _ = require('underscore');

var bcryptHelper = new BcryptHelper();


/** USER APIs */

//add user to db
router.post('/user/register', function(req, res){
    try {
        userService.registerUser(req.body, function (result) {
            if (!result || result === false) {
                next(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });
    } catch (e) {
        var serverResponse = new ServerResponse({}, ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

//logout user i.e removing of user token
router.post('/user/logout', function (req, res, next) {
    try {
        userService.logout(req.body, function (result) {
            if (!result || result === false) {
                next(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });
    } catch (e) {
        var serverResponse = new ServerResponse({}, ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

//update user api, in which input is flat user object
router.post('/user/update', function (req, res) {
    try {
        userService.updateUser(req.body, function (result) {
            if (!result || result === false) {
                next(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });
    } catch (e) {
        var serverResponse = new ServerResponse({}, ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

//user login api call.
router.post('/user/login', function(req, res, next) {
    try {
        userService.validateLogin(req.body, function (isValid, result) {
            if (!isValid) {
                return res.send(new ServerResponse(result, ResponseCodes.BadRequest, Messages.BadRequest));
            } else {
                 passport.authenticate('local', {session: false}, function (err, user, info) {
                    req.user = user;
                    if (err) {
                        return next(err);
                    }
                    if (!user) {
                        return res.send(new ServerResponse({}, ResponseCodes.BadRequest, info));
                    }
                    return next();
                })(req, res, next);
            }
        });
    } catch (e) {
            var serverResponse = new ServerResponse({}, ResponseCodes.InternalServerError, Messages.Error);
            res.send(serverResponse);
            return;
    }
    }, utilityHelper.generateToken , function (req, res) {
        try {
            var data = {
                user: req.user,
                token: req.token
            };
            userService.updateUserToken(data.user, data.token, function (result) {
                if (!result.error && result.data) {
                    var serverResponse = new ServerResponse(result.data, ResponseCodes.Ok, Messages.Success);
                    res.send(serverResponse);
                } else {
                    var serverResponse = new ServerResponse({}, ResponseCodes.BadRequest, Messages.BadRequest);
                    res.send(serverResponse);
                }
            });

        } catch (e) {
            var serverResponse = new ServerResponse({}, ResponseCodes.InternalServerError, Messages.Error);
            res.send(serverResponse);
            return;
        }
});

//EO - USER APIs


/** Chat APIs */

router.get('/chat/list', function (req, res) {
    try {
        chatService.getChats(req.query, function (result) {
            if (!result || result === false) {
                res.send(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });

    } catch (e) {
        var serverResponse = new ServerResponse('', ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

router.get('/chat/message', function (req, res) {
    try {
        chatService.getChatMessages(req.query, function (result) {
            if (!result || result === false) {
                res.send(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });

    } catch (e) {
        var serverResponse = new ServerResponse('', ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

router.get('/chat/:id', function (req, res) {
    try {
        var data = req.params.id;
        chatService.getChat(data, function (result) {
            if (!result || result === false) {
                res.send(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });

    } catch (e) {
        var serverResponse = new ServerResponse('', ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

router.post('/chat/markasread/', function (req, res) {
    try {
        var data = req.body;
        chatService.markAsRead(data, function (result) {
            if (!result || result === false) {
                next(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });
    } catch (e) {
        var serverResponse = new ServerResponse('', ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

router.get('/chat/unread/count', function (req, res) {
    try {
        var data = req.query;
        chatService.getUnReadMessagesCount(data, function (result) {
            if (!result || result === false) {
                next(new ServerResponse({}, ResponseCodes.Forbidden, Messages.Forbidden));
            } else {
                res.send(result);
                return;
            }
        });
    } catch (e) {
        var serverResponse = new ServerResponse('', ResponseCodes.InternalServerError, Messages.Error);
        res.send(serverResponse);
        return;
    }
});

//testing the server
router.get('/', function (req, res) {
    res.json(200, {msg: 'API Working!' });
});

module.exports = router;