const _ = require('underscore');

function UserModel() {
    UserModel.prototype.login = function (userInfo) {
        this.userInfo = {
            email: userInfo.email,
            password: userInfo.password
        }
        return this.userInfo;
    };
    UserModel.prototype.loginRA = function () {
        this.userInfo = {
            required: {
                email: true,
                password: true
            }
        }
        return this.userInfo;
    };

    UserModel.prototype.userSocketInfo = function (userInfo) {
        this.userSocketInfo = {
            socket: userInfo.socket,
            // isActive: userInfo.isActive
        };

        return this.userSocketInfo;
    };

    UserModel.prototype.userSocketInfoRA = function () {
        this.userSocketInfoRA = {
            required: {
                socket: false,
                // isActive: false
            }
        };

        return this.userSocketInfoRA;
    };

    UserModel.prototype.userInfo = function (userInfo) {
        this.userInfo = {
            firstName: userInfo.firstName,
            lastName: userInfo.lastName,
            email       : userInfo.email, 
            gender    : userInfo.gender,
            password: userInfo.password
        }
        return this.userInfo;
    };

    UserModel.prototype.descriptiveUserInfo = function (userInfo) {
        this.descriptiveUserInfo = {
            firstName: userInfo.firstName,
            lastName: userInfo.lastName,
            email       : userInfo.email, 
            gender    : userInfo.gender,
            password: userInfo.password,
            characterInfo : {
                modelId: userInfo.modelId,
                upperBodyTexture: userInfo.upperBodyTexture,
                lowerBodyTexture: userInfo.lowerBodyTexture,
                shoesTexture: userInfo.shoesTexture,
                skinTexture: userInfo.skinTexture,
                attribute1: userInfo.attribute1,
                attribute2: userInfo.attribute2,
                attribute3: userInfo.attribute3,
                attribute4: userInfo.attribute4,
                skinTone: userInfo.skinTone
            },
            details: {
                credits: userInfo.credits,
                status: userInfo.status
            },
            roomDetails: {
                furnitures: userInfo.furnitures
            }
        }
        return this.descriptiveUserInfo;
    }

    UserModel.prototype.userInfoRA = function () {
        this.userInfoRA = {
            required: {
                firstName: true,
                lastName: true,
                email   : true, 
                gender  : true,
                password: true
            }
            
        }
        return this.userInfoRA;
    };

    UserModel.prototype.updateUserInfo = function (modelInfo, data) {
        this.updateUserInfo = modelInfo;
        for (var key in modelInfo) {
            if (modelInfo[key] instanceof Object) {
                for (var subKey in modelInfo[key]) {
                    if (!_.isUndefined(data[subKey])) {
                        this.updateUserInfo[key][subKey] = data[subKey];
                    }
                }
            } else if (!_.isEmpty(data[key])) {
                this.updateUserInfo[key] = data[key];
            } else {
                delete this.updateUserInfo[key];
            }
        }
        return this.updateUserInfo;
    };
} //UserModel function hoisted at the top

exports.UserModel = UserModel;