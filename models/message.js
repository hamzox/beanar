
function MessageModel() {

    MessageModel.prototype.messageInfo = function (messageInfo) {
        this.messageInfo = {
            to: messageInfo.to,
            from: messageInfo.from,
            message: messageInfo.message,
            chat: messageInfo.chat,
            isRead: (messageInfo.isRead) ? messageInfo.isRead : 0
        };

        return this.messageInfo;
    };

    MessageModel.prototype.messageInfoRA = function () {
        this.messageInfoRA = {
            required: {
                to: true,
                from: true,
                message: true,
                chat: true,
                isRead: false
            }
        };

        return this.messageInfoRA;
    };

};

exports.MessageModel = MessageModel;