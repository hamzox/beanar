var bcrypt = require('bcrypt-nodejs');

function BcryptHelper() {

    BcryptHelper.prototype.comparePassword = function (actualPassword, userPassword, callback) {
        bcrypt.compare(actualPassword, userPassword, function (err, isMatch) {
            callback(isMatch, err);
        });
    }

    BcryptHelper.prototype.encryptPassword = function(password, callback) {
        bcrypt.genSalt(1, function (err, salt) {
            if (err) {
                return callback(null, err);
            }
            bcrypt.hash(password, salt, null, function (err, hash) {
                if (err) {
                    return callback(null, err);
                }
                callback(hash, null);
            });
        });
    }

}

exports.BcryptHelper = BcryptHelper;