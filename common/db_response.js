var _ = require('underscore');

function DatabaseResponse(error, data) {
    this.data = (_.isArray(data) && data.length == 1) ? data[0] : data;
    this.error = error;
}

exports.DatabaseResponse = DatabaseResponse;