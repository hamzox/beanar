var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var DatabaseResponse = require('../common/db_response.js').DatabaseResponse;
var _ = require('underscore');

var ChatSchema = new Schema({
    lastMessage: {type: String},
    createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    users: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'User'
    }],
    unRead: [{
        user: {type: String},
        count: {type: Number}
    }]
}, {timestamps: true});

ChatSchema.plugin(mongoosePaginate);
var chat = mongoose.model('Chat', ChatSchema);


function findAllWithPagination(searchParams, options, callback) {
    options.populate = [{
        path: 'users',
        populate: {
            path: 'image'
        }
    }];
    chat.paginate(searchParams, options, function (err, users) {
        if (err) {
            var databaseResponse = new DatabaseResponse(err, null);
            return callback(databaseResponse);
        }
        var databaseResponse = new DatabaseResponse(null, users);
        return callback(databaseResponse);
    });
};

function findAll(searchParams, callback) {
    chat.find(searchParams)
        .populate({
            path: 'users',
            populate: {
                path: 'image'
            }
        }).exec(function (err, users) {
        if (err) {
            var databaseResponse = new DatabaseResponse(err, null);
            return callback(databaseResponse);
        }
        var databaseResponse = new DatabaseResponse(null, users);
        return callback(databaseResponse);
    });
}

function Chat() {

    Chat.prototype.findById = function (id, callback) {
        chat.find({
            _id: id
        }).populate({
            path: 'users',
            populate: {
                path: 'image'
            }
        }).exec(function (err, chat) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, chat);
            callback(databaseResponse);
        });
    };

    Chat.prototype.findOne = function (params, callback) {
        chat.findOne(params, function (err, chat) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, chat);
            callback(databaseResponse);
        });
    };

    Chat.prototype.findAll = function (searchParams, options, callback) {
        if (!_.isEmpty(options)) {
            findAllWithPagination(searchParams, options, callback);
        } else {
            findAll(searchParams, callback);
        }
    };

    Chat.prototype.create = function (chatInfo, callback) {
        var newChat = new chat(chatInfo);
        newChat.save(function (err, chat) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, chat);
            callback(databaseResponse);
        });
    };

    Chat.prototype.update = function (chatInfo, data, callback) {
        chat.findOneAndUpdate({_id: chatInfo.id}, data, {new: true}, function (err, chat) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            var databaseResponse = new DatabaseResponse(null, chat);
            callback(databaseResponse);
        });
    };

    Chat.prototype.delete = function (id, callback) {
        chat.find({
            _id: id
        }, function (err, chat) {
            if (err) {
                var databaseResponse = new DatabaseResponse(err, null);
                return callback(databaseResponse);
            }
            chat.remove(function (err) {
                if (err) {
                    var databaseResponse = new DatabaseResponse(err, null);
                    return callback(databaseResponse);
                }
                var databaseResponse = new DatabaseResponse(null, {});
                callback(databaseResponse);
            });
        });
    };

    Chat.prototype.getCount = function (data, callback) {
        chat.aggregate(
            {
                $unwind: "$unRead"
            },
            {
                $match: {'unRead.user': data.userId, 'unRead.count': {$gt: 0}}
            },
            {
                $group: {
                    _id: null,
                    totalMessages: {$sum: 1},
                    count: {$sum: 1}
                }
            }, function (err, res) {
                var databaseResponse = new DatabaseResponse(err, res);
                callback(databaseResponse);
            });
    };

};

exports.Chat = Chat;