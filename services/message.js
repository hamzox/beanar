'use strict';

var MessageModel = require('../models/message.js').MessageModel;
var messageRepository = new (require('../repository/message.js')).MessageRepository();
var validator = new (require('../validations/validator_request.js')).Validator();

function MessageService() {

    MessageService.prototype.create = function (data, callback) {

        var messageModel = new MessageModel();

        var messageInfo = messageModel.messageInfo(data);
        var messageInfoRA = messageModel.messageInfoRA();
        var result = validator.validate(messageInfo, messageInfoRA);

        if (!!result && result.validationResult.length > 0) {
            if (callback) {
                callback(null, result.validationResult);
            } else {
                throw result;
            }
        } else {

            messageRepository.create(messageInfo, function (databaseResponse) {
                return callback(databaseResponse);
            });
        }
    };

}
exports.MessageService = MessageService;