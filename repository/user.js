var user = new (require('../dal/user.js')).User();

function UserRepository() {
    UserRepository.prototype.create = function (userInfo, callback) {
        try {
            user.create(userInfo, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    UserRepository.prototype.update = function (userInfo, data, callback) {
        try {
            user.update(userInfo, data, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    UserRepository.prototype.findOne = function (params, callback) {
        try {
            user.findOne(params, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    UserRepository.prototype.findById = function (id, callback) {
        try {
            user.findById(id, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };
}

exports.UserRepository = UserRepository;