'use strict';

function ChatModel() {

    ChatModel.prototype.chatInfo = function (chatInfo) {
        this.chatInfo = {
            lastMessage: chatInfo.lastMessage,
            createdBy: chatInfo.createdBy,
            users: chatInfo.users,
            unRead: []
        };

        for (var index = 0; index < chatInfo.users.length; index++) {
            if (chatInfo.users[index].toString() == chatInfo.to) {
                this.chatInfo.unRead.push({
                    user: chatInfo.to,
                    count: 1
                });
            } else {
                this.chatInfo.unRead.push({
                    user: chatInfo.from,
                    count: 0
                });
            }
        }

        return this.chatInfo;
    };

    ChatModel.prototype.chatInfoRA = function () {
        this.chatInfoRA = {
            required: {
                lastMessage: true,
                createdBy: true,
                users: true,
                // unRead:
            }
        };

        return this.chatInfoRA;
    };

    /*
     * @chatInfo - Chat from DB
     * @chatUpdateInfo - New message from request
     * return chatUpdateInfo
     */
    ChatModel.prototype.chatUpdateInfo = function (chatInfo, chatUpdateInfo) {
        this.chatUpdateInfo = {
            lastMessage: chatUpdateInfo.lastMessage,
            unRead: []
        };

        if (chatInfo.unRead && chatInfo.unRead.length > 0) {
            for (var index = 0; index < chatInfo.unRead.length; index++) {
                if (chatInfo.unRead[index].user == chatUpdateInfo.to) {
                    this.chatUpdateInfo.unRead.push({
                        user: chatUpdateInfo.to,
                        count: chatInfo.unRead[index].count + 1
                    });
                } else {
                    this.chatUpdateInfo.unRead.push(chatInfo.unRead[index]);
                }
            }
        } else {
            this.chatUpdateInfo.unRead.push({
                user: chatUpdateInfo.to,
                count: 1
            });
            this.chatUpdateInfo.unRead.push({
                user: chatUpdateInfo.from,
                count: 0
            });
        }

        return this.chatUpdateInfo;
    };

    ChatModel.prototype.markAsRead = function (chatInfo) {
        this.markAsRead = {
            user: chatInfo.user,
            chatId: chatInfo.chatId
        };

        return this.markAsRead;
    };

    ChatModel.prototype.markAsReadRA = function () {
        this.markAsReadRA = {
            required: {
                user: true,
                chatId: true
            }
        };

        return this.markAsReadRA;
    };

    ChatModel.prototype.getUnReadObject = function (chatData, chatInfo) {

        this.chatUpdateData = {
            unRead: []
        };

        for (var index = 0; index < chatInfo.unRead.length; index++) {
            if (chatData.user == chatInfo.unRead[index].user) {
                this.chatUpdateData.unRead.push({
                    user: chatData.user,
                    count: 0
                });
            } else {
                this.chatUpdateData.unRead.push(chatInfo.unRead[index]);
            }
        }

        return this.chatUpdateData;

    };

    ChatModel.prototype.getUnReadChatCountInfo = function (getUnReadChatCountInfo) {
        this.getUnReadChatCountInfo = {
            userId: getUnReadChatCountInfo.userId
        };

        return this.getUnReadChatCountInfo;
    };

    ChatModel.prototype.getUnReadChatCountInfoRA = function () {
        this.getUnReadChatCountInfoRA = {
            required: {
                userId: true
            }
        };

        return this.getUnReadChatCountInfoRA;
    };

};

exports.ChatModel = ChatModel;