var ValidationResponse = require('../common/server_response.js').ValidationResponse;
var ValidationResultList = require('../common/server_response.js').ValidationResultList;
var Messages = require('../common/constant.js').Messages;
var util = require('util');

function Validator() {

    Validator.prototype.validate = function(requestData, requiredStatus) {
        var validationResponses = [];

        if(requestData){
            for(var key in requiredStatus.required){
                if(requiredStatus.required[key] instanceof Object){
                    if(requestData[key]) {
                        requestData[key] = (requestData[key] instanceof Object) ? requestData[key] : JSON.parse(requestData[key]);
                        requestData[key] = (requestData[key] instanceof Array) ? requestData[key] : [requestData[key]];
                        for (var index = 0; index < requestData[key].length; index++) {
                            var validationRespList = this.validate(requestData[key][index], requiredStatus.required[key]);
                            validationResponses = validationResponses.concat(validationRespList.validationResult);
                        }
                    } else{
                        validationResponses.push(new ValidationResponse(key, 'true', util.format(Messages.Required, key)));
                    }
                } else if(requiredStatus.required[key]) {
                    if (!requestData[key] || requestData[key].length <= 0) {
                        validationResponses.push(new ValidationResponse(key, 'true', util.format(Messages.Required, key)));
                    }
                }
            }
        } else {
            validationResponses.push(new ValidationResponse("Request Param", 'true', util.format(Messages.Required, "Request Param")));
        }
        var validationResultList = new ValidationResultList(validationResponses);
        return validationResultList;
    };

}

exports.Validator = Validator;