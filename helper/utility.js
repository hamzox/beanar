
const jwt = require('jsonwebtoken');
const _ = require('underscore');
const userService = new (require('../services/user.js')).UserService();
const ServerResponse = require('../common/server_response.js').ServerResponse;
const ResponseCodes = require('../common/constant.js').ResponseCodes;
const Messages = require('../common/constant.js').Messages;
let instance = null;

function UtilityHelper() {

    UtilityHelper.prototype.generateToken = function (req, res, next) {
        req.token = jwt.sign({
            id: req.user._id,
        }, "define_server_secret");
        next();
    };

    
    UtilityHelper.prototype.authenticate = function (req, res, next) {
        var token = "";
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            token = req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            token = req.query.token;
        }

        if (token) {
            userService.getUser({token: token}, function (response) {
                if (!response.error && !_.isEmpty(response.data)) {
                    req.user = response.data;
                    next();
                } else {
                    var response = new ServerResponse({}, ResponseCodes.UnAuthorized, Messages.UnAuthorized);
                    res.send(response);
                }
            });
        } else {
            var response = new ServerResponse({}, ResponseCodes.UnAuthorized, Messages.UnAuthorized);
            res.send(response);
        }
    };
}

exports.intance = function () {
    if (!instance) {
        instance = new UtilityHelper();
    }
    return instance;
};