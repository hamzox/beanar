var _ = require('underscore');

function QueryHelper() {

    QueryHelper.prototype.queryBuilder = function (condition, elements, model) {

    };

    QueryHelper.prototype.getPaginationParams = function(queryParams) {
        var paginationParams = {};
        if(!_.isEmpty(queryParams.page) && !_.isEmpty(queryParams.limit)){
            paginationParams = {
                page: parseInt(queryParams.page),
                limit: parseInt(queryParams.limit)
            };
        }
        return paginationParams;
    };

}

exports.QueryHelper = QueryHelper;