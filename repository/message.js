var message = new (require('../dal/message')).Message();

function MessageRepository() {

    MessageRepository.prototype.findById = function (id, callback) {
        try {
            message.findById(id, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    MessageRepository.prototype.findOne = function (params, callback) {
        try {
            message.findOne(params, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    MessageRepository.prototype.findAll = function (searchParams, options, callback) {
        try {
            message.findAll(searchParams, options, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    MessageRepository.prototype.getList = function (callback) {
        try {
            message.getList(function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    MessageRepository.prototype.create = function (messageInfo, callback) {
        try {
            message.create(messageInfo, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    MessageRepository.prototype.update = function (messageInfo, data, callback) {
        try {
            message.update(messageInfo, data, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

    MessageRepository.prototype.delete = function (id, callback) {
        try {
            message.delete(id, function(databaseResponse){
                callback(databaseResponse);
            });
        } catch(err) {
            throw err;
        }
    };

};

exports.MessageRepository = MessageRepository;