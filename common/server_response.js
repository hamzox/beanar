/**
 * Generic Server Response Model
 * @param data
 * @param code
 * @param message
 * @constructor
 */
function ServerResponse(data, code, message) {
    this.version = "1.0";
    this.data = data;
    this.code = code;
    this.message = message;
}

exports.ServerResponse = ServerResponse;

/**
 * creates a validation response
 * @param field
 * @param isValid
 * @param message
 * @constructor
 */
function ValidationResponse(field, isValid, message) {
    this.field = field;
    this.isValid = isValid;
    this.message = message;
}

exports.ValidationResponse = ValidationResponse;

function ValidationResultList(validationResponse) {
    this.validationResult = validationResponse;
}

exports.ValidationResultList = ValidationResultList;